//
//  ViewController.h
//  CustomizeAppearance
//
//  Created by Dato on 3/1/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@end
