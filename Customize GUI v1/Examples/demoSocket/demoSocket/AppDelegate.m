//
//  AppDelegate.m
//  demoSocket
//
//  Created by Dato on 3/15/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "AppDelegate.h"
@interface AppDelegate()<NSStreamDelegate>
{
    NSInputStream * inputStream;
    NSOutputStream * outputStream;
}
@end
@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"192.168.1.46", 445, &readStream, &writeStream);
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    inputStream.delegate = self;
    outputStream.delegate = self;
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
    // Override point for customization after application launch.
    return YES;
}

#pragma mark - NSStreamDelegate
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    if (aStream == inputStream) {
        switch (eventCode) {
            case NSStreamEventOpenCompleted:
            {
                NSLog(@"open completed__input");
            }
                break;
            case NSStreamEventHasBytesAvailable:{
                NSLog(@"byte available__input");
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len >= 0) {
                        NSData * output = [NSData dataWithBytes:buffer length:len];
                        
                        if (nil != output) {
                            [self receiveData:output];
                        }
                    }
                }
            }
                break;
            case NSStreamEventErrorOccurred:{
                NSLog(@"Can not connect to the host__input");
                NSLog(@"%@",aStream.streamError);
            }
                break;
            case NSStreamEventEndEncountered:{
                [aStream close];
                [aStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            }
                break;              
                
            default:
                break;
        }
    }else if(aStream == outputStream){
        switch (eventCode) {
            case NSStreamEventOpenCompleted:
            {
                NSLog(@"open completed__output");
            }
                break;
            case NSStreamEventHasSpaceAvailable:{
                NSLog(@"space available__output");
                if (step == 0) {
                    [self sendNegociateRequest];
                }
            }
                break;
            case NSStreamEventErrorOccurred:{
                NSLog(@"Can not connect to the host__output");
                NSLog(@"%@",aStream.streamError);
            }
                break;
            case NSStreamEventEndEncountered:{
                [aStream close];
                [aStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)writeHexString:(NSString *)hexString{
    NSString * commandString = [hexString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSMutableData *commandToSend= [[NSMutableData alloc] init];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [commandString length]/2; i++) {
        byte_chars[0] = [commandString characterAtIndex:i*2];
        byte_chars[1] = [commandString characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [commandToSend appendBytes:&whole_byte length:1];
    }
    [outputStream write:[commandToSend bytes] maxLength:commandToSend.length];
}

#pragma mark - Send functions
- (void) sendNegociateRequest{
    NSString * command = @"0000002fff534d4272000000001803c80000000000000000000000000000902e00000100000c00024e54204c4d20302e313200";
    [self writeHexString:command];
    step ++;
}

- (void) sendSessionSetupAndxRequest{
    NSString * command = @"00000088ff534d4273000000001807c80000000000000000000000000000248f000002000cff00dede04410a000100000000002d0000000000541000804d004e544c4d535350000100000015b2086001000100200000000c000c00210000003f4a43494653315f34365f3132570069006e0064006f00770073002000380000006a0043004900460053000000";
    [self writeHexString:command];
    step ++;
}
- (void) sendSessionSetupAndxRequest2{
    NSString * command = @"000001ceff534d4273000000001807c800004253525350594c200000000089d6000803000cff00dede04410a000100000000007201000000005410008093014e544c4d53535000030000001800180040000000e600e60058000000020002003e0100000a000a0040010000180018004a0100001000100062010000158208600166f483af5075ffc90f5436c31ecfaa113f60347059e467666c12733b798ab3a4229f431d72fb40010100000000000010e6a2c31a22ce0123c47648ea3d1df7000000000200140054004500430048004d00410053005400450052000100140048004f005300540053004500520056004500520004001c0074006500630068006d00610073007400650072002e0063006f006d000300320048006f00730074005300650072007600650072002e0074006500630068006d00610073007400650072002e0063006f006d0005001c0074006500630068006d00610073007400650072002e0063006f006d0007000800383272801b22ce0100000000000000003f00470055004500530054004a00430049004600530031005f00340036005f003200330083ee78f6d829439c1ea515c5aca484a900570069006e0064006f00770073002000380000006a0043004900460053000000";
    [self writeHexString:command];
    step ++;
}


#pragma mark - Receive functions
int step = 0;
- (void) receiveData:(NSData *)data{
    NSLog(@"data for step:%d \n%@",step, data);
    switch (step) {
        case 1:
            [self sendSessionSetupAndxRequest];
            break;
            
        case 2:
            [self sendSessionSetupAndxRequest2];
            break;
            
        default:
            break;
    }
}

@end
