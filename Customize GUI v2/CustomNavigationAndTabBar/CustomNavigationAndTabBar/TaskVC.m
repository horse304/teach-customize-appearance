//
//  TaskVC.m
//  CustomNavigationAndTabBar
//
//  Created by Dato on 7/15/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "TaskVC.h"

@interface TaskVC ()

@end

@implementation TaskVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Task Completed";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
