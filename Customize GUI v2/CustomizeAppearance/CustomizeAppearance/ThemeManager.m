//
//  ThemeManager.m
//  CustomizeAppearance
//
//  Created by Dato on 3/1/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "ThemeManager.h"

@implementation ThemeManager
UIColor * baseColor;
+ (void)loadTheme1{
    baseColor = [UIColor colorWithRed:219.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme2{
    baseColor = [UIColor colorWithRed:166.0/255.0 green:197.0/255.0 blue:118.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme3{
    baseColor = [UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:98.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme4{
    baseColor = [UIColor colorWithRed:78.0/255.0 green:151.0/255.0 blue:197.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}

+ (void)updateAppearanceWithBaseColor{
    [[UINavigationBar appearance] setTintColor:baseColor];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"background_bar.jpg"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 0, 7, 0)] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"background_bar.jpg"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 0, 7, 0)] forBarMetrics:UIBarMetricsLandscapePhone];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor redColor],
                                                          UITextAttributeTextColor,
                                                          [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
                                                          UITextAttributeTextShadowColor,
                                                          [NSValue valueWithUIOffset:UIOffsetMake(2, 2)],
                                                          UITextAttributeTextShadowOffset, 
                                                          [UIFont fontWithName:@"AppleGothic" size:0.0],
                                                          UITextAttributeFont, 
                                                          nil]];

    UIImage *segmentSelected =
    [[UIImage imageNamed:@"segcontrol_sel.png"]
     resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    UIImage *segmentUnselected =
    [[UIImage imageNamed:@"segcontrol_uns.png"]
     resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    UIImage *segmentSelectedUnselected =
    [UIImage imageNamed:@"segcontrol_sel-uns.png"];
    UIImage *segUnselectedSelected =
    [UIImage imageNamed:@"segcontrol_uns-sel.png"];
    UIImage *segmentUnselectedUnselected =
    [UIImage imageNamed:@"segcontrol_uns-uns.png"];
    
    [[UISegmentedControl appearance] setBackgroundImage:segmentUnselected
                                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:segmentSelected
                                               forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setDividerImage:segmentUnselectedUnselected
                                 forLeftSegmentState:UIControlStateNormal
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setDividerImage:segmentSelectedUnselected
                                 forLeftSegmentState:UIControlStateSelected
                                   rightSegmentState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] 
     setDividerImage:segUnselectedSelected 
     forLeftSegmentState:UIControlStateNormal 
     rightSegmentState:UIControlStateSelected 
     barMetrics:UIBarMetricsDefault];
    
//    [[UISlider appearance] setMinimumTrackTintColor:baseColor];
    [[UISwitch appearance] setOnTintColor:baseColor];
    [[UIActivityIndicatorView appearance] setColor:baseColor];
    [[UIStepper appearance] setTintColor:baseColor];
    [[UIProgressView appearance] setProgressTintColor:baseColor];
}
@end
