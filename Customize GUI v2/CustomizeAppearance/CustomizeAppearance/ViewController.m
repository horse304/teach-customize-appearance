//
//  ViewController.m
//  CustomizeAppearance
//
//  Created by Dato on 3/1/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//
#import "AppDelegate.h"
#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *myBarButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *myButton;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UITextField *myTextfield;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mySegmentedControl;
@property (weak, nonatomic) IBOutlet UISlider *mySlider;
@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myActivityIndicator;
@property (weak, nonatomic) IBOutlet UIStepper *myStepper;
@property (weak, nonatomic) IBOutlet UIProgressView *myProgress;
@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.mySlider setMinimumTrackImage:[[UIImage imageNamed:@"slider_minimum"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 5, 4, 0)] forState:UIControlStateNormal];
    [self.mySlider setMaximumTrackImage:[[UIImage imageNamed:@"slider_maximum"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 0, 4, 5)] forState:UIControlStateNormal];
    [self.mySlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)togglePickerView:(id)sender {
    self.myPickerView.hidden = !self.myPickerView.hidden;
    if (self.myPickerView.hidden == TRUE) {
        [(UIButton *)sender setTitle:@"Show Picker" forState:UIControlStateNormal];
    }else{
        [(UIButton *)sender setTitle:@"Hide Picker" forState:UIControlStateNormal];
    }
}

#pragma mark - PickerView Datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 4;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat:@"Theme %d",row + 1];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    UIColor * baseColor;
    switch (row) {
        case 0:
        {
            baseColor = [UIColor colorWithRed:219.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1.0];
        }
            break;
        case 1:
        {
            baseColor = [UIColor colorWithRed:166.0/255.0 green:197.0/255.0 blue:118.0/255.0 alpha:1.0];
        }
            break;
        case 2:
        {
            baseColor = [UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:98.0/255.0 alpha:1.0];
        }
            break;
        case 3:
        {
            baseColor = [UIColor colorWithRed:78.0/255.0 green:151.0/255.0 blue:197.0/255.0 alpha:1.0];
        }
            break;
            
            
        default:
            break;
    }
    //Set tint color for navigation bar
    self.navigationController.navigationBar.tintColor = baseColor;
    //Set tint color for button
    self.myBarButtonItem.tintColor = baseColor;
    self.myButton.tintColor = baseColor;
    self.mySegmentedControl.tintColor = baseColor;
    self.segmented2.tintColor = [UIColor yellowColor];
    self.mySlider.minimumTrackTintColor = baseColor;
    self.mySwitch.onTintColor = baseColor;
    self.myActivityIndicator.color = baseColor;
    self.myStepper.tintColor = baseColor;
    self.myProgress.progressTintColor = baseColor;
}

@end
