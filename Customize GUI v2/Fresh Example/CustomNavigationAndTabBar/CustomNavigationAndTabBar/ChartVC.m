//
//  ViewController.m
//  CustomNavigationAndTabBar
//
//  Created by Dato on 7/15/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "ChartVC.h"
#import "LineChartVC.h"

@interface ChartVC ()

@property (strong, nonatomic) LineChartVC * lineChartVC;

@end

@implementation ChartVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Chart";
	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)touchLineChart:(id)sender {
    self.lineChartVC = [[LineChartVC alloc] initWithNibName:@"LineChartVC" bundle:nil];
    [self.navigationController pushViewController:self.lineChartVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
