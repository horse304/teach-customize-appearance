//
//  ThemeManager.h
//  CustomizeAppearance
//
//  Created by Dato on 3/1/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemeManager : NSObject
+ (void)loadTheme1;
+ (void)loadTheme2;
+ (void)loadTheme3;
+ (void)loadTheme4;

@end
