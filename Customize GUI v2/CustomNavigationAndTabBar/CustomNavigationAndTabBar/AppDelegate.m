//
//  AppDelegate.m
//  CustomNavigationAndTabBar
//
//  Created by Dato on 7/15/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "AppDelegate.h"

#import "ChartVC.h"
#import "TaskVC.h"
#import "EventVC.h"

@interface AppDelegate ()


@property (strong, nonatomic) ChartVC *chartVC;
@property (strong, nonatomic) TaskVC *taskVC;
@property (strong, nonatomic) EventVC *eventVC;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [self createAppearance];
    
    self.chartVC = [[ChartVC alloc] initWithNibName:@"ChartVC_iPhone" bundle:nil];
    self.taskVC = [[TaskVC alloc] initWithNibName:@"TaskVC" bundle:nil];
    self.eventVC = [[EventVC alloc] initWithNibName:@"EventVC" bundle:nil];
    UITabBarController * tabBarController = [[UITabBarController alloc] init];
    [tabBarController setViewControllers:@[[[UINavigationController alloc] initWithRootViewController:self.chartVC],
                                           [[UINavigationController alloc] initWithRootViewController:self.taskVC],
                                           [[UINavigationController alloc] initWithRootViewController:self.eventVC]]];
    
    [(UITabBarItem *)tabBarController.tabBar.items[0] setTitle:@"Chart"];
    [(UITabBarItem *)tabBarController.tabBar.items[0] setFinishedSelectedImage:[UIImage imageNamed:@"chart_icon.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"chart_icon_unselect.png"]];
    [(UITabBarItem *)tabBarController.tabBar.items[1] setTitle:@"Task"];
    [(UITabBarItem *)tabBarController.tabBar.items[1] setFinishedSelectedImage:[UIImage imageNamed:@"task_icon_select.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"task_icon.png"]];
    [(UITabBarItem *)tabBarController.tabBar.items[2] setTitle:@"Event"];
    [(UITabBarItem *)tabBarController.tabBar.items[2] setFinishedSelectedImage:[UIImage imageNamed:@"event_icon_select.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"event_icon.png"]];
    
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)createAppearance{
    
    //Create apperance for UINavigationBar
    UIImage * navBgImage = [[UIImage imageNamed:@"nav_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 0, 3)];
    [[UINavigationBar appearance] setBackgroundImage:navBgImage forBarMetrics:UIBarMetricsDefault ];//Set Navigation bar background
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc]init]];//Disable shadow of uinavigation bar
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor:[UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:243.0/255.0 alpha:1.0],
                                     UITextAttributeFont:[UIFont fontWithName:MU_FONT_HelveticaNeue_Bold size:20.0],
                          UITextAttributeTextShadowColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0],
                         UITextAttributeTextShadowOffset:[NSValue valueWithUIOffset:UIOffsetMake(0, 0)]}];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[[UIImage imageNamed:@"nav_back_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(14.0, 12.0, 14.0, 6.0)]
                                                      forState:UIControlStateNormal
                                                    barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor:[UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:243.0/255.0 alpha:1.0],
                                     UITextAttributeFont:[UIFont fontWithName:MU_FONT_HelveticaNeue size:15.0],
                          UITextAttributeTextShadowColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0],
                         UITextAttributeTextShadowOffset:[NSValue valueWithUIOffset:UIOffsetMake(0, 0)]}
     
                                                forState:UIControlStateNormal];
    
    //Create apperance for UITabBar
    [[UITabBar appearance] setBackgroundImage:[[UIImage imageNamed:@"tab_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage imageNamed:@"tab_selection_indicator_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc]init]];//Disable shadow
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                UITextAttributeTextColor:[UIColor colorWithRed:94.0/255.0 green:109.0/255.0 blue:129.0/255.0 alpha:1.0],
                                     UITextAttributeFont:[UIFont fontWithName:MU_FONT_Courier size:10.0]}
                                          forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                          UITextAttributeTextColor:[UIColor colorWithRed:20.0/255.0 green:185.0/255.0 blue:214.0/255.0 alpha:1.0],
                               UITextAttributeFont:[UIFont fontWithName:MU_FONT_Courier size:10.0]}
                                          forState:UIControlStateSelected];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
