//
//  ThemeManager.m
//  CustomizeAppearance
//
//  Created by Dato on 3/1/13.
//  Copyright (c) 2013 Techmaster. All rights reserved.
//

#import "ThemeManager.h"

@implementation ThemeManager
UIColor * baseColor;
+ (void)loadTheme1{
    baseColor = [UIColor colorWithRed:219.0/255.0 green:97.0/255.0 blue:97.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme2{
    baseColor = [UIColor colorWithRed:166.0/255.0 green:197.0/255.0 blue:118.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme3{
    baseColor = [UIColor colorWithRed:255.0/255.0 green:163.0/255.0 blue:98.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}
+ (void)loadTheme4{
    baseColor = [UIColor colorWithRed:78.0/255.0 green:151.0/255.0 blue:197.0/255.0 alpha:1.0];
    [self updateAppearanceWithBaseColor];
}

+ (void)updateAppearanceWithBaseColor{
    [[UINavigationBar appearance] setTintColor:baseColor];
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"background_bar.jpg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 0, 10,0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                          [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0],
//                                                          UITextAttributeTextColor,
//                                                          [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
//                                                          UITextAttributeTextShadowColor,
//                                                          [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
//                                                          UITextAttributeTextShadowOffset, 
//                                                          [UIFont fontWithName:@"Arial-Bold" size:0.0], 
//                                                          UITextAttributeFont, 
//                                                          nil]];

    [[UIButton appearance] setTintColor:baseColor];
    [[UISegmentedControl appearance] setTintColor:baseColor];
    [[UISlider appearance] setMinimumTrackTintColor:baseColor];
    [[UISwitch appearance] setOnTintColor:baseColor];
    [[UIActivityIndicatorView appearance] setColor:baseColor];
    [[UIStepper appearance] setTintColor:baseColor];
    [[UIProgressView appearance] setProgressTintColor:baseColor];
}
@end
